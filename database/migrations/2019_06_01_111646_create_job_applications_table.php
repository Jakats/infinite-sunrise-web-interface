<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identifier');
            $table->json('answers');
            $table->boolean('isAccepted')->default(false);
            $table->boolean('isRejected')->default(false);
            $table->string('rejectionReason')->nullable();
            $table->string('bossIdentifier')->nullable();
            $table->boolean('initiated')->default(false);
            $table->string('job');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_applications');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCriminalRecordsOfficerId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('criminal_records', function (Blueprint $table) {
            $table->dropColumn('officer_id');
            $table->integer('police_officer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('criminal_records', function (Blueprint $table) {
            $table->dropColumn('police_officer_id');
            $table->string('officer_id');
        });
    }
}

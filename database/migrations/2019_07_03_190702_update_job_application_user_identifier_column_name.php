<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateJobApplicationUserIdentifierColumnName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('job_applications', function (Blueprint $table) {
            $table->renameColumn('identifier', 'user_identifier');
            $table->integer('web_user_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('job_applications', function (Blueprint $table) {
            $table->renameColumn('user_identifier', 'identifier');
            $table->removeColumn('web_user_id');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraFieldsForPoliceReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('criminal_records', function (Blueprint $table) {
            $table->string('place')->nullable();
            $table->string('car_plate')->nullable();
            $table->string('other_parties')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('criminal_records', function (Blueprint $table) {
            $table->dropColumn('place');
            $table->dropColumn('car_plate');
            $table->dropColumn('other_parties');
        });
    }
}

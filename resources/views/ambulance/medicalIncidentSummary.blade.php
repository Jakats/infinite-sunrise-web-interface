@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.searchBar', ['routeName' => 'ambulanceSearchUser', 'placeholder' => __('forms.search_user')])
        @endcomponent
        <h2 class="text-gray">Medical Record #{{$incident->id}}</h2>
        <div class="card">
 			<div class="card-body">
                <p><b>Name:</b> {{$incident->characterName}}
                    ({{$incident->user()->first()->name}})</p>
                <p><b>Summary:</b> {{$incident->summary}}</p>
                <p><b>Issuer:</b> {{\App\Helpers\UserHelper::getCharacterName($incident->emsWorker()->first()->user()->first())}}</p>
                <a class="btn btn-info d-inline-block" href="{{URL::previous()}}" onclick="overlayOn()">
                    {{__('buttons.back_to_previous_page')}}
                </a>
                @if(\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->identifier === $incident->emsWorker()->first()->user_identifier)
                    <form method="POST" action="{{route('deleteMedicalIncident')}}" onsubmit="return ConfirmDelete()"
                          class="d-inline-block">
                        @csrf
                        <input type="hidden" name="medicalIncidentId" value="{{$incident->id}}">
                        <button type="submit" class="btn btn-danger" onclick="overlayOn()">{{__('buttons.delete')}}</button>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection
<script>

    /**
     *
     * @returns {boolean}
     * @constructor
     */
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>

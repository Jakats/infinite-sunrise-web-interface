<div class="overlay" id="legendOverlay" onclick="hideLegend()">
    <div class="container center">
        <div class="row justify-content-md-center">
            <div class="card col-md-6 mouse-over-regular justify-content-md-center">
                <div class="card-header text-center font-weight-bold">
                    {{ __('headers.help') }}
                </div>
                <div class="card-body h-100">
                    <div class="row align-items-center">
                        <div class="col-md-2 text-center">
                        <i class="far fa-money-bill-alt fa-2x text-success d-block"></i>
                        </div>
                        <div class="col-md-10 mx-auto">
                            <span class="text-dark mx-auto">{{ __('texts.cash') }}</span>
                        </div>
                    </div>
                    <div class="row align-items-center mt-3">
                        <div class="col-md-2 text-center">
                            <i class="far fa-credit-card fa-2x text-success d-block"></i>
                        </div>
                        <div class="col-md-10 mx-auto">
                            <span class="text-dark mx-auto">{{ __('texts.money_on_account') }}</span>
                        </div>
                    </div>
                    <div class="row align-items-center mt-3">
                        <div class="col-md-2 text-center">
                            <i class="fas fa-file-invoice-dollar fa-2x text-success d-block"></i>
                        </div>
                        <div class="col-md-10 mx-auto">
                            <span class="text-dark mx-auto">{{ __('texts.bills_tota_ammount') }} </span>
                        </div>
                    </div>
                    <div class="row align-items-center mt-3">
                        <div class="col-md-2 text-center">
                            <div class="progress" style="background-color: #b9bbbe;">
                                <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="col-md-10 mx-auto">
                            <span class="text-dark mx-auto">{{ __('texts.billPolice') }} </span>
                        </div>
                    </div>
                    <div class="row align-items-center mt-3">
                        <div class="col-md-2 text-center">
                            <div class="progress" style="background-color: #b9bbbe;">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="col-md-10 mx-auto">
                            <span class="text-dark mx-auto">{{ __('texts.billAmbulance') }} </span>
                        </div>
                    </div>
                    <div class="row align-items-center mt-3">
                        <div class="col-md-2 text-center">
                            <div class="progress" style="background-color: #b9bbbe;">
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="col-md-10 mx-auto">
                            <span class="text-dark mx-auto">{{ __('texts.billTaxi') }} </span>
                        </div>
                    </div>
                    <div class="row align-items-center mt-3">
                        <div class="col-md-2 text-center">
                            <div class="progress" style="background-color: #b9bbbe;">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="col-md-10 mx-auto">
                            <span class="text-dark mx-auto">{{ __('texts.billMechanic') }} </span>
                        </div>
                    </div>
                    <div class="row align-items-center mt-3">
                        <div class="col-md-2 text-center">
                            <div class="progress" style="background-color: #b9bbbe;">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="col-md-10 mx-auto">
                            <span class="text-dark mx-auto">{{ __('texts.billCarDealer') }} </span>
                        </div>
                    </div>
                    <div class="row align-items-center mt-3">
                        <div class="col-md-2 text-center">
                            <div class="progress" style="background-color: #b9bbbe;">
                            </div>
                        </div>
                        <div class="col-md-10 mx-auto">
                            <span class="text-dark mx-auto">{{ __('texts.other_bills') }} </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

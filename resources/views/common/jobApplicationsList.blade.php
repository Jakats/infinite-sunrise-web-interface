@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">Steam name</th>
                    <th scope="col">First name</th>
                    <th scope="col">Last name</th>
                    <th scope="col">Discord Name</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($applications as $application)
                    <tr>
                        <th scope="row">{{ $application->user()->first()->name }}</th>
                        <th>{{ $application->user()->first()->firstname }}</th>
                        <th>{{ $application->user()->first()->lastname }}</th>
                        <th>{{ json_decode($application->answers)->discordName }}</th>
                        <th>
                            <a class="btn btn-info"
                               href="{{route($singleApplicationRoute, ['applicationId' => $application->id])}}">
                                {{__('buttons.open')}}
                            </a>
                        </th>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @if(method_exists($applications, 'links'))
                <div class="container">
                    <div class="pagination justify-content-center p-4">
                        {{$applications->links()}}
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header justify-content-center"><b>{{__('headers.police_job_application')}}</b></div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('jobApplicationStore') }}">
                            @csrf
                            <input type="hidden" name="job" value="police">
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-6">
                                    <label for="discordName">{{ __('jobApplications.discordName') }}</label>
                                    <input type="text"
                                           class="form-control {{ $errors->has('discordName') ? ' is-invalid' : '' }}"
                                           id="discordName" placeholder="{{__('jobApplications.discordName')}}"
                                           name="discordName" value="{{ old('discordName') }}" required autofocus>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="age">{{ __('jobApplications.age') }}</label>
                                    <input type="number"
                                           class="form-control {{ $errors->has('age') ? ' is-invalid' : '' }}"
                                           id="age" placeholder="{{ __('jobApplications.age') }}" name="age"
                                           value="{{ old('age') }}" required autofocus>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-6">
                                    <p>{{ __('jobApplications.copInAnotherServer') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="copInAnotherServer"
                                               id="copInAnotherServerYes" value="{{ __('jobApplications.true') }}">
                                        <label class="form-check-label" for="copInAnotherServerYes">
                                            {{ __('jobApplications.true') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="copInAnotherServer"
                                               id="copInAnotherServerNo" value="{{ __('jobApplications.false') }}" checked>
                                        <label class="form-check-label" for="copInAnotherServerNo">
                                            {{ __('jobApplications.false') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <p>{{ __('jobApplications.playingTimes') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="playingTimes"
                                               id="playingEveryDay" value="{{ __('jobApplications.every_day') }}" checked>
                                        <label class="form-check-label" for="playingEveryDay">
                                            {{ __('jobApplications.every_day') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="playingTimes"
                                               id="playingAfterWork" value="{{ __('jobApplications.every_day_after_work_school') }}">
                                        <label class="form-check-label" for="playingAfterWork">
                                            {{ __('jobApplications.every_day_after_work_school') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="playingTimes"
                                               id="playingWeekends" value="{{ __('jobApplications.only_weekends') }}">
                                        <label class="form-check-label" for="playingWeekends">
                                            {{ __('jobApplications.only_weekends') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="playingTimes"
                                               id="playingOther"
                                               value="{{ __('jobApplications.cant_answer_depends_on_how_much_time_I_have_after_work_and_personal_life') }}">
                                        <label class="form-check-label" for="playingOther">
                                            {{ __('jobApplications.cant_answer_depends_on_how_much_time_I_have_after_work_and_personal_life') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-6">
                                    <label for="otherServers">{{ __('jobApplications.otherServers') }}</label>
                                    <textarea
                                        class="form-control {{ $errors->has('otherServers') ? ' is-invalid' : '' }}"
                                        id="otherServers" placeholder="{{ __('jobApplications.other_servers') }}" name="otherServers"
                                        required autofocus>{{ old('otherServers') }}</textarea>
                                </div>
                                <div class="form-group col-md-6">
                                    <label
                                        for="previousExperience">{{ __('jobApplications.previousExperience') }}</label>
                                    <textarea
                                        class="form-control {{ $errors->has('previousExperience') ? ' is-invalid' : '' }}"
                                        id="previousExperience" placeholder="{{ __('jobApplications.previous_experience') }}"
                                        name="previousExperience" required
                                        autofocus>{{ old('previousExperience') }}</textarea>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-6">
                                    <p>{{ __('jobApplications.irlExperiencePolice') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="irlExperiencePolice"
                                               id="irlExperienceYes" value="{{ __('jobApplications.true') }}">
                                        <label class="form-check-label" for="irlExperienceYes">
                                            {{ __('jobApplications.true') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="irlExperiencePolice"
                                               id="irlExperienceNo" value="{{ __('jobApplications.false') }}" checked>
                                        <label class="form-check-label" for="irlExperienceNo">
                                            {{ __('jobApplications.false') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <p>{{ __('jobApplications.microphone') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="microphone"
                                               id="microphoneYes" value="{{ __('jobApplications.true') }}" checked>
                                        <label class="form-check-label" for="microphoneYes">
                                            {{ __('jobApplications.true') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="microphone" id="microphoneNo"
                                               value="{{ __('jobApplications.false') }}">
                                        <label class="form-check-label" for="microphoneNo">
                                            {{ __('jobApplications.false') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-6">
                                    <label
                                        for="characterDescription">{{ __('jobApplications.characterDescription') }}</label>
                                    <textarea
                                        class="form-control {{ $errors->has('characterDescription') ? ' is-invalid' : '' }}"
                                        id="characterDescription" placeholder="{{ __('jobApplications.characterDescription') }}"
                                        name="characterDescription" required autofocus>
                                            {{ old('characterDescription') }}
                                    </textarea>
                                </div>
                                <div class="form-group col-md-6">
                                    <label
                                        for="whyYouWantToBePolice">{{ __('jobApplications.whyYouWantToBePolice') }}</label>
                                    <textarea
                                        class="form-control {{ $errors->has('whyYouWantToBePolice') ? ' is-invalid' : '' }}"
                                        id="whyYouWantToBePolice" placeholder="{{ __('jobApplications.whyYouWantToBePolice') }}"
                                        name="whyYouWantToBePolice" required autofocus>
                                            {{ old('whyYouWantToBePolice') }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-12">
                                    <label for="whenYouUseGun">{{ __('jobApplications.whenYouUseGun') }}</label>
                                    <textarea
                                        class="form-control {{ $errors->has('whenYouUseGun') ? ' is-invalid' : '' }}"
                                        id="whenYouUseGun" placeholder="{{ __('jobApplications.whenYouUseGun') }}"
                                        name="whenYouUseGun" required autofocus>
                                            {{ old('whenYouUseGun') }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-12">
                                    <label for="trafficStop">{{ __('jobApplications.trafficStop') }}</label>
                                    <textarea
                                        class="form-control {{ $errors->has('trafficStop') ? ' is-invalid' : '' }}"
                                        id="trafficStop" placeholder="{{ __('jobApplications.describe_your_actions') }}"
                                        name="trafficStop" required autofocus>
                                            {{ old('trafficStop') }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-12">
                                    <p>{{ __('jobApplications.question1') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="question1" id="q1a1"
                                               value="{{ __('jobApplications.will_shoot_criminal_with_my_firearm') }}" checked>
                                        <label class="form-check-label" for="q1a1">
                                            {{ __('jobApplications.will_shoot_criminal_with_my_firearm') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="question1" id="q1a2"
                                               value="{{ __('jobApplications.will_hit_criminal_with_a_car') }}">
                                        <label class="form-check-label" for="q1a2">
                                            {{ __('jobApplications.will_hit_criminal_with_a_car') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="question1" id="q1a3"
                                               value="{{ __('jobApplications.will_drive_as_close_as_possible_exit_a_car_and_start_chase_criminal_on_foot_If_possible_i_will_use_taser_to_stop_criminal') }}">
                                        <label class="form-check-label" for="q1a3">
                                            {{ __('jobApplications.will_drive_as_close_as_possible_exit_a_car_and_start_chase_criminal_on_foot_If_possible_i_will_use_taser_to_stop_criminal') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="question1" id="q1a4"
                                               value="{{ __('jobApplications.will_run_next_to_the_criminal_and_cuff_him_her_while_he_she_is_still_running') }}">
                                        <label class="form-check-label" for="q1a4">
                                            {{ __('jobApplications.will_run_next_to_the_criminal_and_cuff_him_her_while_he_she_is_still_running') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="question1" id="q1a5"
                                               value="{{ __('jobApplications.will_stop_chasing_and_i_will_go_back_to_patrol') }}">
                                        <label class="form-check-label" for="q1a5">
                                            {{ __('jobApplications.will_stop_chasing_and_i_will_go_back_to_patrol') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-12">
                                    <p>{{ __('jobApplications.question2') }}</p>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="question2" id="q2a1"
                                               value="{{ __('jobApplications.will_shoot_him_because_why_not') }}"
                                               checked>
                                        <label class="form-check-label" for="q2a1">
                                            {{ __('jobApplications.will_shoot_him_because_why_not') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="question2" id="q2a1"
                                               value="{{ __('jobApplications.will_take_out_a_firearm_and_aim_it_to_man_who_holds_a_knife_i_will_demand_him_to_put_down_a_knife_and_raise_his_hands_up') }}">
                                        <label class="form-check-label" for="q2a1">
                                            {{ __('jobApplications.will_take_out_a_firearm_and_aim_it_to_man_who_holds_a_knife_i_will_demand_him_to_put_down_a_knife_and_raise_his_hands_up') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="question2" id="q2a1"
                                               value="{{ __('jobApplications.will_run_back_to_my_car_and_hit_both_man_with_my_car') }}">
                                        <label class="form-check-label" for="q2a1">
                                            {{ __('jobApplications.will_run_back_to_my_car_and_hit_both_man_with_my_car') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="question2" id="q2a1"
                                               value="{{ __('jobApplications.will_run_away_because_i_am_scared_of_my_life') }}">
                                        <label class="form-check-label" for="q2a1">
                                            {{ __('jobApplications.will_run_away_because_i_am_scared_of_my_life') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="question2" id="q2a1"
                                               value="{{ __('jobApplications.will_watch_a_fight_because_it_is_fun') }}">
                                        <label class="form-check-label" for="q2a1">
                                            {{ __('jobApplications.will_watch_a_fight_because_it_is_fun') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <input type="submit" class="btn btn-success" id="confirmButton" onclick="overlayOn()"
                                   value="{{__('buttons.submit_application')}}" disabled>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    window.onload = function () {
        const discordNameField = document.getElementById('discordName');
        const ageField = document.getElementById('age');
        const otherServersField = document.getElementById('otherServers');
        const previousExperienceField = document.getElementById('previousExperience');
        const characterDescriptionField = document.getElementById('characterDescription');
        const whyYouWantToBePoliceField = document.getElementById('whyYouWantToBePolice');
        const whenYouUseGunField = document.getElementById('whenYouUseGun');
        const trafficStopField = document.getElementById('trafficStop');
        const confirmButton = document.getElementById('confirmButton');

        let elementArray = [
            discordNameField,
            ageField,
            otherServersField,
            previousExperienceField,
            characterDescriptionField,
            whyYouWantToBePoliceField,
            whenYouUseGunField,
            trafficStopField,
        ];

        elementArray.forEach(function (element) {
            element.addEventListener('keyup', function (event) {
                let isValidDiscordNameField = discordNameField.checkValidity();
                let isValidAgeField = ageField.checkValidity();
                let isValidOtherServersField = otherServersField.checkValidity();
                let isValidPreviousExperienceField = previousExperienceField.checkValidity();
                let isValidCharacterDescriptionField = characterDescriptionField.checkValidity();
                let isValidWhyYouWantToBePoliceField = whyYouWantToBePoliceField.checkValidity();
                let isValidWhenYouUseGunField = whenYouUseGunField.checkValidity();
                let isValidTrafficStopField = trafficStopField.checkValidity();

                if (
                    isValidDiscordNameField
                    && isValidAgeField
                    && isValidOtherServersField
                    && isValidPreviousExperienceField
                    && isValidCharacterDescriptionField
                    && isValidCharacterDescriptionField
                    && isValidWhyYouWantToBePoliceField
                    && isValidWhenYouUseGunField
                    && isValidTrafficStopField
                ) {
                    confirmButton.disabled = false;
                } else {
                    confirmButton.disabled = true;
                }
            });
        });
    }
</script>

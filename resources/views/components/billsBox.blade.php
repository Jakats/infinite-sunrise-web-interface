@component('components.box')
<div onClick="showBillsTable()" class="mouse-over">
    <div class="row">
        <div class="col-md-12 text-center mt-5 mb-4">
            <i class="fas fa-file-invoice-dollar  fa-3x text-success text-center"></i>
            <div class="mt-1 align-middle font-weight-bold" style="color: #1b1e21; font-size: 20px;">{{number_format($billsExtraData['billSum'], 0, '.', ' ')}}</div>
        </div>
    </div>
    @if ($billsExtraData['billCount'])
    <div class="row">
        <div class="col-md-12 mt-3">
            <div class="progress" style="background-color: #b9bbbe;" title="{{__('texts.other_bills')}}: {{ $billsExtraData['billOther']->count() }}">
                <div class="progress-bar" role="progressbar" style="width: {{\App\Helpers\BillsHelper::getPrecentOf($billsExtraData['billPolice'], $billsExtraData['billCount'])}}%"
                aria-valuenow="{{ $billsExtraData['billPolice']->count() }}" aria-valuemin="0" aria-valuemax="{{ $billsExtraData['billCount'] }}"
                title="{{__('texts.billPolice')}}: {{ $billsExtraData['billPolice']->count() }}"></div>

                <div class="progress-bar bg-danger" role="progressbar" style="width: {{\App\Helpers\BillsHelper::getPrecentOf($billsExtraData['billAmbulance'], $billsExtraData['billCount'])}}%"
                aria-valuenow="{{ $billsExtraData['billPolice']->count() }}" aria-valuemin="0" aria-valuemax="{{ $billsExtraData['billCount'] }}"
                title="{{__('texts.billAmbulance')}}: {{ $billsExtraData['billAmbulance']->count() }}"></div>

                <div class="progress-bar bg-warning" role="progressbar" style="width: {{\App\Helpers\BillsHelper::getPrecentOf($billsExtraData['billTaxi'], $billsExtraData['billCount'])}}%"
                aria-valuenow="{{ $billsExtraData['billTaxi']->count() }}" aria-valuemin="0" aria-valuemax="{{ $billsExtraData['billCount'] }}"
                title="{{__('texts.billTaxi')}}: {{ $billsExtraData['billTaxi']->count() }}"></div>

                <div class="progress-bar bg-success" role="progressbar" style="width: {{\App\Helpers\BillsHelper::getPrecentOf($billsExtraData['billMechanic'], $billsExtraData['billCount'])}}%"
                aria-valuenow="{{ $billsExtraData['billMechanic']->count() }}" aria-valuemin="0" aria-valuemax="{{ $billsExtraData['billCount'] }}"
                title="{{__('texts.billMechanic')}}: {{ $billsExtraData['billMechanic']->count() }}"></div>

                <div class="progress-bar bg-info" role="progressbar" style="width: {{\App\Helpers\BillsHelper::getPrecentOf($billsExtraData['billCarDealer'], $billsExtraData['billCount'])}}%"
                aria-valuenow="{{ $billsExtraData['billCarDealer']->count() }}" aria-valuemin="0" aria-valuemax="{{ $billsExtraData['billCount'] }}"
                title="{{__('texts.billCarDealer')}}: {{ $billsExtraData['billCarDealer']->count() }}"></div>
          </div>
      </div>
    </div>
    @endif
</div>
<div class="overlay" id="billsTable" onClick="hideBillsTable()">
    <div class="container center">
        <div class="row justify-content-md-center">
            <div class="card">
            @component('components.bills', ['bills' => $bills, 'identifier' => $userData->identifier])
            @endcomponent
            </div>
        </div>
    </div>
</div>
@endcomponent

<div class="pb-5">
    <form class="form-inline" action="{{route($routeName)}}" method="get">
        <input type="text" class="form-control col-md-9" id="search" name="search"
               placeholder="{{__('forms.search_car_sell_request')}}">
        <button type="submit" class="btn btn-primary col-md-2 offset-md-1" onclick="overlayOn()"
                onkeypress="return keypress(event)">
            {{__('buttons.search')}}
        </button>
    </form>
</div>
<script>
    function keypress (event) {
        if (event.which == 13 || event.keyCode == 13) {
            overlayOn();
            return false;
        }
        return true;
    };
</script>

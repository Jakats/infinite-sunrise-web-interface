<div class="card-header  mouse-over" data-toggle="collapse"
     href="#characterBody"
     aria-expanded="false"
     aria-controls="characterBody">
    <b>{{__('headers.characters')}}</b>
    <span class="badge badge-pill badge-secondary">{{\App\Character::where('identifier', $identifier)->get()->count()}}</span>
</div>
<div class="card-body collapse" id="characterBody">
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">{{__('tables.character_name')}}</th>
            <th>{{__('tables.date_of_birth')}}</th>
            @if($police)
                <th>{{__('tables.is_wanted')}}</th>
                <th>{{__('tables.wanted_reason')}}</th>
                <th>{{__('tables.declared_wanted_by')}}</th>
                <th>{{__('tables.date')}}</th>
                <th>{{__('tables.picture')}}</th>
            @endif
            @if($actions)
                <th scope="col">{{__('tables.actions')}}</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($characters as $character)
                <th scope="row">{{$character->firstname}} {{$character->lastname}}</th>
                <td>{{$character->dateofbirth}}</td>
                @if($police)
                    <td>
                        @if (\App\Helpers\WantedHelper::isWantedByCharacter($character->firstname.$character->lastname, $wanted))
                            True
                        @else
                            False
                        @endif
                    </td>
                    <td>
                        @if (\App\Helpers\WantedHelper::isWantedByCharacter($character->firstname.$character->lastname, $wanted))
                            {{\App\Helpers\WantedHelper::getWantedReason($character->firstname.$character->lastname, $wanted)}}
                        @endif
                    </td>
                    <td>
                        @if (\App\Helpers\WantedHelper::isWantedByCharacter($character->firstname.$character->lastname, $wanted))
                            {{\App\Helpers\WantedHelper::getCreatorName($character->firstname.$character->lastname, $wanted)}}
                        @endif
                    </td>
                    <td>
                        @if (\App\Helpers\WantedHelper::isWantedByCharacter($character->firstname.$character->lastname, $wanted))
                            {{\App\Helpers\WantedHelper::getCreationTime($character->firstname.$character->lastname, $wanted)}}
                        @endif
                    </td>
                        @if (\App\Helpers\WantedHelper::isWantedByCharacter($character->firstname.$character->lastname, $wanted))
                        <td class="mouse-over" onclick="pictureOverlayOn('{{\App\Helpers\WantedHelper::getImageName($character->firstname.$character->lastname, $wanted)}}')">
                            <a href="#">
                                {{\App\Helpers\WantedHelper::getImageName($character->firstname.$character->lastname, $wanted)}}
                            </a>
                        </td>
                    @else
                        <td></td>
                        @endif

                @endif
                @if($actions)
                    <td>
                        @if($police)
                            @if (!\App\Helpers\WantedHelper::isWantedByCharacter($character->firstname.$character->lastname, $wanted))
                                <form method="POST" action="{{route('policeDeclareAsFugitive')}}"
                                      enctype="multipart/form-data" onsubmit="return ConfirmDelete()">
                                    @csrf
                                    <input type="hidden" name="characterName"
                                           value="{{$character->firstname}} {{$character->lastname}}">
                                    <input type="hidden" name="userSteamId"
                                           value="{{$character->identifier}}">
                                    <label for="reason">Reason</label>
                                    <textarea id="reason" name="reason" class="form-control"></textarea>
                                    <label for="picture">Picture</label>
                                    <input type="file" name="picture">
                                    <button type="submit" class="btn btn-danger mt-2" onclick="overlayOn()">
                                        {{__(__('buttons.declare_as_fugitive'))}}
                                    </button>
                                </form>
                            @else
                                <form method="POST" action="{{route('policeUnDeclareAsFugitive')}}"
                                      onsubmit="return ConfirmDelete()">
                                    @csrf
                                    <input type="hidden" name="wantedId"
                                           value="{{\App\Helpers\WantedHelper::getWantedId($character->firstname.$character->lastname, $wanted)}}">
                                    <button type="submit" class="btn btn-warning" onclick="overlayOn()">
                                        {{__('buttons.un_declare_as_fugitive')}}
                                    </button>
                                </form>
                            @endif
                        @endif
                    </td>
                    @endif
                    </td>
            </tr>
            @isset($wanted)
                <div class="overlay" id="pictureOverlay{{\App\Helpers\WantedHelper::getImageName($character->firstname.$character->lastname, $wanted)}}"
                     onclick="pictureOverlayOff('{{\App\Helpers\WantedHelper::getImageName($character->firstname.$character->lastname, $wanted)}}')">
                    <img class="center" src="{{'/images/'. \App\Helpers\WantedHelper::getImageName($character->firstname.$character->lastname, $wanted)}}">
                </div>
            @endisset
        @endforeach
        </tbody>
    </table>
</div>

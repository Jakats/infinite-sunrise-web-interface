<table class="table">
    <thead class="thead-light">
    <tr>
        @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
            <th scope="col">{{__('tables.steam_name')}}</th>
        @endif
        <th scope="col">{{__('tables.call_sign')}}</th>
        <th scope="col">{{__('tables.character_name')}}</th>
        <th scope="col">{{__('tables.date_of_birth')}}</th>
        <th scope="col">{{__('tables.sex')}}</th>
        <th scope="col">{{__('tables.phone')}}</th>
        <th scope="col">{{__('tables.job_grade')}}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr class="mouse-over"
            onclick="
                window.location='{{route($routeName, [$user->identifier])}}';
                overlayOn();
                ">
            @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                <td>{{ $user->name }}</td>
            @endif
            <td>
                @if ($user->policeOfficerCallSign()->first())
                    {{ $user->policeOfficerCallSign()->first()->callSign }}
                @endif
            </td>
            <td>{{ \App\Helpers\UserHelper::getCharacterName($user) }}</td>
            <td>{{ $user->dateofbirth }}</td>
            <td>{{ $user->sex }}</td>
            <td>{{ $user->phone_number }}</td>
            <td>{{ $ranks->where('grade', $user->job_grade)->first()->label  }}</td>
        </tr>
    @endforeach
    </tbody>
    @if($auxiliaryPolice)
        <tbody>
        @foreach($auxiliaryPolice as $user)
            <tr class="mouse-over"
                onclick="
                    window.location='{{route($routeName, [$user->identifier])}}';
                    overlayOn();
                    ">
                @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                    <td>{{ $user->name }}</td>
                @endif
                <td>
                    @if ($user->policeOfficerCallSign()->first())
                        {{ $user->policeOfficerCallSign()->first()->callSign }}
                    @endif
                </td>
                <td>{{ \App\Helpers\UserHelper::getCharacterName($user) }}</td>
                <td>{{ $user->dateofbirth }}</td>
                <td>{{ $user->sex }}</td>
                <td>{{ $user->phone_number }}</td>
                <td>{{ __('texts.auxiliaryPolice')  }}</td>
            </tr>
        @endforeach
        </tbody>
    @endif
</table>

@extends('user.layout.layout')
@section('userBody')
    <div class="row row-eq-height mt-5 d-flex" style="height: 160px">
        <div class="col-md-6 mouse-over h-100" onclick="showProfileUpload()">
            @component('components.userDataBox', ['user' => $userData, 'profilePicture' => $profilePicture])
            @endcomponent
        </div>
        <div class="col-md-6 h-100">
            @component('components.moneyBox', ['data' => $userData])
            @endcomponent
        </div>
    </div>
    <div class="row row-eq-height mt-5 d-flex" style="height: 250px;">
        <div class="col-md-6">
          @component('components.billsBox', ['billsExtraData' => $billsExtraData, 'bills' => $bills, 'userData' => $userData])
          @endcomponent
        </div>
        @if(isset($userData->job_grade) and $userData->job_grade >= 90)
            <div class="col-md-6">
              @component('components.companyBox', ['companyMoney' => $companyInfo['money'], 'companyName' => $companyInfo['name']])
              @endcomponent
            </div>
        @endif
    </div>
    <div class="row row-eq-height mt-5 d-flex" style="height: 250px;">
        <div class="col-md-6">
          @component('components.skillsBox', ['skills' => $skills])
          @endcomponent
        </div>
    </div>
    <div class="card mt-5">
        @component('components.licenses', ['licenses' => $licenses, 'identifier' => $userData->identifier])
        @endcomponent
        @component('components.properties', ['ownedProperties' => $ownedProperties, 'identifier' => $userData->identifier])
        @endcomponent
    </div>

    @if(in_array($userData->job, \App\Http\Controllers\PoliceController::ALLOWED_JOBS))
        <button class="mt-3 btn btn-success" data-toggle="collapse"
                href="#officerCallSignForm" aria-expanded="false"
                aria-controls="officerCallSignForm">
            {{__('buttons.set_call_sign')}}
        </button>
        <div class="card mt-3 collapse" id="officerCallSignForm">
            <div class="card-body">
                <form method="POST" action="{{route('setCallSign')}}"
                      class="mt-3 mb-3" onsubmit="overlayOn()">
                    @csrf
                    <input type="hidden" name="userIdentifier" value="{{$userData->identifier}}">
                    <label for="callSign">{{ __('forms.call_sign') }}:</label>
                    <input type="text" id="callSign" name="callSign" class="form-control"
                           placeholder="{{ __('forms.call_sign') }}" required>
                    <button type="submit" class="btn btn-info mt-3"
                            onclick="overlayOn()" id="confirmButtonChangeName">
                        {{__('buttons.confirm')}}
                    </button>
                </form>
            </div>
        </div>
    @endif
@endsection
<div class="overlay" id="profileUpdate">
    <div class="container center">
        <div class="row justify-content-md-center">
            <div class="card col-md-6 mouse-over-regular justify-content-md-center">
                <div class="card-header text-center font-weight-bold">
                    {{__('headers.update_profile')}}
                    <i class="far fa-times-circle fa-2x float-md-right mouse-over" style="color: #fe7985;" onclick="hideProfileUpload()"></i>
                </div>
                <div class="card-body align-content-md-center justify-content-md-center text-center">
                    <form method="POST" action="{{route('homeUploadProfilePicture')}}" enctype="multipart/form-data" onsubmit="overlayOn()"
                    class="align-content-md-center justify-content-md-center text-center">
                        @csrf
                        <label for="carImage" class="mt-2">{{__('forms.profile_picture')}}</label>
                        <input type="file" name="image" id="carImage" class="d-block btn-dark btn col-md-12">
                        <button type="submit" class="btn btn-info mt-3"
                                id="confirmButtonWanted">
                            {{__('buttons.confirm')}}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    /**
     * @returns {boolean}
     * @constructor
     */
    function ConfirmSell() {
        var x = confirm("Are you sure you want to continue?");
        if (x)
            return true;
        else
            return false;
    }
</script>

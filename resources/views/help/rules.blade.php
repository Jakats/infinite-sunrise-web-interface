@extends('help.layout.layout')
@section('helpHeader')
    <strong>Serveri Reeglid</strong>
@endsection
@section('helpBody')
    <div>
        <div>
            <h4><strong>Sisukord</strong></h4>
            <ul>
                <li><a href="#Üldreeglid">Üldreeglid</a></li>
                <li><a href="#Liiklusreeglid">Liiklusreeglid</a></li>
                <li><a href="#Surm_ja_tapmine">Surm ja tapmine</a></li>
                <li><a href="#Röövid">Röövid</a></li>
                <li><a href="#Greenzone">Greenzone</a></li>
                <li><a href="#Töö">Töö</a></li>
                <li><a href="#Dokumendid">Dokumendid</a></li>
                <li><a href="#Relvad">Relvad</a></li>
                <li><a href="#Discordi_reeglid">Discordi reeglid</a></li>
                <li><a href="#Gangide_Reeglid">Gangide Reeglid</a></li>
            </ul>
        </div>
        <h4 id="Üldreeglid"><strong>Üldreeglid</strong></h4>
        <ol>
            <li>15 minutit ennem tormi(serveri restarti) on KRIMINAALSETE tegevuste tegemine keelatud!</li>
            <li>Mikrofoni olemasolu esmatähtis!</li>
            <li>FailRP keelatud!</li>
            <li>Metagaming rangelt keelatud! (Mitte RP käigus saadud info enda kasuks ära kasutamine.)</li>
            <li>RDM/VDM keelatud! Sinna alla läheb ka admin vanglas tapmine ja peksmine (Ka NPCide tapmine, näiteks narko müügil või nende röövimisel liigutub RDMi alla)</li>
            <li>Trollimine keelatud!</li>
            <li>Värviline Steam nimi on keelatud.</li>
            <li>Buggide ärakasutamine rangelt keelatud!</li>
            <li>Discordi Voice chati kasutamine keelatud! Suhtlemiseks on telefon, millega saab helistada ja sõnumeid saata! Lisaks on ka Raadiosaatjad olemas müügil</li>
            <li>Kui üks rehv on täielikult katki ja rehvi pole all tuleb koheselt seisma jääda, mitte edasi sõita.</li>
            <li>Keelatud on kuulsate inimeste nimede kasutamine (Ott Tänak, Tanel Padar vms) ehk siis identiteedivargus! Karistuseks 24 bänn!</li>
            <li>Narko kohtade müümine on keelatud!</li>
            <li>Steam nimes reklaam keelatud! (ala mingi cstrade või twitch.tv)</li>
            <li>Peale serveri restarti / crashi ei või ilma ADMINI / VALITSUSE loata serverisse logida.</li>
            <li>Politsei, Kiirabi, FBI riiete kasutamine tavakodanikel keelatud! (Jalakabuur, vööpeal kabuur kaasa arvatud!)</li>
            <li>Maski / Kiivri kasutamine on keelatud! (Autopood ega mehaanik ei teeninda maski või kiivrit kandvat inimest) Maski / Kiivrit võib kasutada ainult röövide ajal! Narko tootmise / müümise ajal ei või inimesed kasutada maski! Näokate on lubatud.</li>
            <li>Värviliste nimede ja teksti kasutamine chatis keelatud.</li>
            <li>Austa serveri juhtkonda ja nende otsuseid.</li>
            <li>Austa serveris mängivaid naisi.</li>
            <li>Illegaalsetes tegevustes võib osa võtta maksimaalselt 5 inimest. ( Narko korjamine, Narko protsessimine, Inimeste röövimine, Poe/panga Röövimine jne )</li>
            <li>Surmanuhtlus: Kui karakterile on määratud 30 päeva jooksul enam kui 300 minutit vanglat on linnavalitsusel õigus määrata isikule surmanuhtlus. Ehk siis kratakteri tapmine, isik jääb ilma kogu varast ja asjadest mis talle kuulusid.</li>
            <li>Breaking character - Keelatud (Out of Character jutuajamine voice chatis)</li>
            <li>Kui inimene on vigastatud, ehk pikali. On tal lubatud telefoni kasutada ja sõnum saata ainult politseile ja kiirabile. Sõbrale või kellegile kolmandale sõnumi saatmine / helistamine on KEELATUD!</li>
            <li>Kui inimene on vigastatud, on raadiosaatja kasutamine keelatud!</li>
            <li>Kanepi kasvatamine eraldi instantsis keelatud! (ruumid kuhu pead minema sisse läbi markeri/plimbi, näiteks korter</li>
            <li>Driveby keelatud. Juhi istmelt tulistamine täielikult keelatud, selleks peab inimene autost välja tulema. Kaasreisjate kohtadelt tulistamine keelatud kui kiirused on üle (k.a) 100km/h</li>
            <li>Isikut on keelatud "selga" / "õlale" võtta kui asud ise juhikohal ehk roolis olle (Ka /me commandiga pole see lubatud ehk siis /me paneb isiku kõrvalistmele). Samuti kui sõiduk võimaldab sisse istuda ainult neljal isikul, siis viiendaks (/tassi) ei tohi võtta vigastatut, et teda viia haiglasse või mõnda muusse kohta! Mootorrattaga sõites on keelatud isikut "selga" / "õlale" võtta. Vigastatud isikut on lubatud ainult õlale võtta ehk siis kasutades /tassi commandi.</li>
            <li>Kui toimub serveri restart või sul endal jookseb mäng kokku, On Sul kohustus minna ja võtta uus tööauto ja tööriided. Keelatud on teha tööd edasi erariietes ning varastatud või enda sõidukiga. Ka toodangu müügi / palga saamise kohas peate olema tööriietes ja sinna minema tööautoga!</li>
            <li>Keelatud on narko müük tunnelis ehk siis metroos. Kui isik jääb selle tegevusega vahele siis esimesel korral määrata automaatselt 60 minutit vanglakaristust (Karistuse määrab admin. Kui admineid sees pole ning isik teeb seda esimest korda võib automaatselt vanglasse saata ka politsei)</li>
            <li>Hääle järgi tuvastamine on lubatud</li>
            <li>Väljaspool mängu suhtlemine IC informatsioonide kohta on keelatud.</li>
            <li>Keelatud on maanduda helikopteri/õhusõidukiga rohelistesse tsoonidesse või ükskõik millistes avalikes kohtades (va politseinikud ja meedikud eriolukordades).</li>
            <li>Suu kaudu muusika mängimine on lubatud klubides, baarides, väliüritustel (kui oled rahva kokku leppel DJ), omas autos, jõusaalis (kuid see ei tohi häirida kaasmängijate rääkimist). Läbi suu muusikat lastes ei tohi seda olla kuulda üle linna.</li>

        </ol>

        <h4 id="Liiklusreeglid"><strong>Liiklusreeglid</strong></h4>
        <ol>
            <li>Sinised ja punased neonid (põhjavalgustid) on keelatud.</li>
            <li>Linnas on lubatud sõita 80km/h ehk siis 50mp/h.</li>
            <li>Maanteel ja kiirteel on lubatud sõita 120km/h ehk siis 75mp/h.</li>
            <li>Punase tulega on parempööre lubatud.</li>
            <li>Parklas kehtiv õueala reegel.
                <ol>
                  <li>Jalakäija tohib liikuda kogu õueala ulatuses, kuid ei tohi juhti põhjendamatult takistada.</li>
                  <li>Mootorsõiduki kiirus õuealal ei tohi ületada 20km/h.</li>
                  <li>Juht ei tohi õuealal jalakäijat ohustada ega takistada, vajaduse korral tuleb sõiduk seisma jätta.</li>
                  <li>Mootorsõiduk võib õuealale sõita vaid peatumiseks või parkimiseks.</li>
                  <li>Õuealal tohib parkida ainult A- ja B-kategooria sõidukit!</li>
              </ol>
            </li>
            <li>Linnatänavatel on lubatud sõita ainult automaksu ära tasutud sõidukiga. Juhul kui sõiduki eest pole tasutud automaksu on politseil õigus juhti trahvida! Peale kolmandat hoiatustrahvi on politseil õigus sõiduk konfiskeerida lõplikult.</li>
            <li>Kui sõiduk pole registreeritud / automaksu tasutud 30 päeva jooksul, siis on linnavalitsusel õigus sõiduk konfiskeerida.</li>
            <li>Ühe auto Automaks maksab 1500€, mootoratta / rollerid maksavad 500€. Sõiduki automaksu arve väljastatakse teile enne lepingusõlmimist. Juhul kui te pole arvet tasunud on teenindajal õigus lepingu vormistamisest keelduda. Automaks on kohustulik siis kui sa omad 2 või rohkem autot. Automaksuga saavad sind aidata mehaanikud ja politseinikud</li>
            <li>Ühe sõiduki automaks kehtib üks kuu (30 päeva.)</li>
            <li>Automaksu saate tasuda ainult endale kuuluva sõiduki eest.</li>
            <li>Üritused Autodega on keelatud linnapildis sõitmine. (Drag, kardid, vormelid jne.) Adminitel ja politseil on õigus sõiduk konfiskeerida. Nendega on lubatud sõita kas kinnisel territooriumil või ametlikel üritustel.</li>
        </ol>

        <h4 id="Surm_ja_tapmine"><strong>Surm ja tapmine</strong></h4>
        <ol>
            <li>Uue Elu Reegel: Pärast haiglas ärkamist, ei mäleta sinu karakter oma tapjast, absoluutselt midagi nendega seonduvat, ei autosid, maju ega nägusid. Sinu karakter ei mäleta ka teisi tegevusi mis juhtusid 15 minuti vältel enne tapmist. Keelatud on ka RevengeKill (Oma tapja tapmine kättemaksuks selle eest, et ta sind ära tappis, isegi siis kui keegi meenuta sulle varasemat juhtunut).</li>
            <li>Kiirabi tapmine ilma põhjuseta keelatud</li>
            <li>Väärtusta teiste ja enda elu. (Niisama ei tapa ja ei sure)</li>
            <li>Kui sind tulistatakse ükskõik millise relvaga, mis ei ole snaiper, või kui maha laskmisele ei järgne /me ''hukkas'', et sind täielikult ära tappa, siis sind võidakse üles aidata sõbra või kiirabi poolt.</li>
            <li>Hukkamine ilma mõjuva põhjuseta keelatud.</li>
            <li>CK tegemine teisele mängijale:
                <ul>
                    <li>Ck tegemiseks peab saama administraatoritel loa</li>
                    <li>Ck tegemise loa saamisek peab olema isikute vahel pika ajaline rp</li>
                    <li>Ck tegemise loa saamisek peab olema mõjuv ja administraatoritele aru saadav põhjus</li>
                </ul>
            </li>
        </ol>

        <h4 id="Röövid"><strong>Röövid</strong></h4>
        <ol>
            <li>Panga röövimise ajal max. 4 röövlit ja 2 pantvangi! Poe röövimise ajal max 3 röövlit ja 1 pantvang!</li>
            <li>Kahte panka/poodi samaaegselt ei röövi!</li>
            <li>Panga ja poeröövide vahe peab olema 25min!!!</li>
            <li>Swedbanki saab röövida siis kui on 4 politseinikku Nordea panka saab röövida siis kui on 3 politseinikku.</li>
            <li>Poeröövide jaoks on vaja vähemalt 2 politseinikku!</li>
            <li>Inimröövide jaoks on vaja vähemalt 1 politseinikku!</li>
            <li>Panga ja poe röövleid võib politsei tulistada juhul kui röövlid avavad tule esimesena.</li>
            <li>Kui pantvang on täitnud su käsud siis teda ei tapa.</li>
            <li>Politseioperatsiooni ei tulda segama kolmanda isiku poolt. (Politsei võib ära väänata ja admin su piiri taha saata)</li>
            <li>Juveelipoe röövis võib olla maksimaalselt kolm röövlit, pantvange on lubatud ainult üks! Keelatud on nõuda politseilt edumaad!</li>
            <li>Kui toimub rööv, ei tohi minna röövlid poest /pangast välja! Vaid peavad ootama ära politseiniku! Põgenemist võib alustada alles siis kui läbirääkimised politseiga on lõppenud.</li>
            <li>Lihunike/kaevurite või kelle iganes röövimisel tulistamine ei ole röövi initiation (alustamine), kontakti tuleb luua suulisel moel, mitte relvaga lastes.</li>
            <li>Keelatud on röövida OnDuty (töölolevat) Politseid, Kiirabi, Mehaanikut ja Taksojuhti. Kui isik on onduty aga era autoga (pole tunnuseid mille järgi ära tunda et tegu oleks töö autoga) siis on röövimine lubatud</li>
        </ol>

        <h4 id="Greenzone"><strong>Greenzone</strong></h4>
        <ol>
            <li>Greenzones alal on keelatud: relvaga vehkimine, tapmine, peksmine, tulistamine, sõidukite varasatmine, provotseerimine ja ülbitsemine.</li>
            <li>Greenzones aladeks on: Pillbox haigla, Sandy haigla, Paleto haigla, Kesklinna politseijaoskond, Sandy politseijaoskond, Paleto politseijaoskond</li>
        </ol>

        <h4 id="Töö"><strong>Töö</strong></h4>
        <ol>
            <li>Tööd tuleb teha töö autoga!</li>
            <li>Politsei, kiirabi ja mehaaniku autode varastamine on keelatud. Karistuseks KICK, BAN.</li>
            <li>Tööd tehakse tööriietes ning töö autoga.</li>
            <li>Tööl olev politseinik peab olema discordis töökanalis. NB! See kanal pole raadioside jaoks.</li>
            <li>Politsei ja kiirabi ei tegele illegaalsete asjadega tööl olles.</li>
            <li>Politsei ei tegele illegaalsete asjadega töö väilisel ajal. Kiirabile see keelt ei kehti, kuid illegaalseid tegevusi tehes vabal ajal riskib kiirabi lisa karistusega töö andja poolt.</li>
            <li>Firma / Asutuse juhil on õiguse kehtestada töö sisekorra eeskirjad mis ei tohi minna vastuollu serveri reeglitega.</li>
            <li>Kütusevedaja ja puuraidur peab tegema tööd koos haagisega! Haagist ei tohi maha jätta!</li>
            <li>Ehitaja, Veiniäri, Tubakaäri, Lihunik, Rätsepad, Kaendavaja, Sushiäri ei või panna toodangut töö sõiduki pagasiruumi!</li>
        </ol>

        <h4 id="Dokumendid"><strong>Dokumendid</strong></h4>
        <ol>
            <li>Dokumendi tegemisel kasutada normaalseid tähti ja normaalset nime.</li>
            <li>Teise isiku nime ja mainet ei riku. Identiteedivargus keelatud.</li>
        </ol>

        <h4 id="Relvad"><strong>Relvad</strong></h4>
        <ol>
            <li>Suuremad relvad tulevad välja auto pagasist (k.a. kurikas, kanister jne) ja ei vaja me või do commandi. Kui relv tuleb välja mõjalt siis peab kaasnema me või do command</li>
            <li>Väiksemat smg relva ei pea (/me võtab smg autost välja) commandiga välja võtma kui teil on mantel või jope seljas mille õlmaalt saab relva välja tõmmata!</li>
            <li>Lubatud (legaalsed) relvad on vaid need, mis on müügil relvapoes.</li>
        </ol>

        <h4 id="Discordi_reeglid"><strong>Discordi reeglid</strong></h4>
        <ol>
            <li>Juturuumis vaidleme minimaalselt, kui admin (valitsuse liige) käsib lõpetada siis tuleb teda kuulata.</li>
            <li>Igasugune Serveri väline reklaam on keelatud! </li>
            <li>Kui teil on valitsuse abi tarvis siis kirjutage 🆘-abi alla discordis. </li>
            <li>Support all kõrvalistel isikutel abi andmine keelatud!</li>
            <li>Juturuumis (voice) kisamine, sõimamine ja häälemuunduri kasutamine keelatud!</li>
        </ol>

        <h4 id="Gangide_Reeglid"><strong>Gangide Reeglid</strong></h4>
        <ol>
            <li>Ühe gängi minimaalne liikmete arv on 5 ja maksimaalne on 10 liiget!</li>
            <li>Peale CKd on keelatud inimsele liituda uuesti sama gängiga.</li>
            <li>Gangide omavaheline koostöö keelatud. Sinna alla ei kuulu äriline tegevus (näiteks asjade müümine ja ostmine).</li>
            <li>Combat Revive KEELATUD! (st. Seni kuni RP olukord käib ja toimub/on toimunud Active Shooting ning vastas osapoolest on keegi veel elus ei tohi minna ja Reviveda oma meeskonna liiget. Reviveda tohib alles siis kui antud Tsenaarium on Lõppenud. ) </li>
            <li>Isegi Oma Narko kohas mille on Gang omale soetanud ei tohi alustada RolePlay olukorda Laskmisega. Tuleb Esmalt kontakti luua suulisel moel. Kui Tegemist on röövimisega kus isik täidab teie soove on Isiku maha lasmine keelatud. (st. Kui röövid isikult inventoryst midagi ) </li>
            <li>Narko kohtades või igasugustes ilegaalsetes tegevustes peab olema Raadio "SHOUTI" peal.</li>
            <li>Maski kandmine Narko korjamises, Narko pakendamises ning müügis keelatud.</li>
            <li>Gang bait ehk teise gangi sümboolika kandimine on keelatud.</li>
            <li>Gangis lahkumisel/välja heitmisel on gangi pealikul õigus teha lahkujale CK/FPK (Faction Player Kill on nagu PK, aga seda teeb grupeering mingile liikmele keda nad soovivad grupeeringust välja visata. FPK puhul jääb inimesele kogu ta vara alles, aga ta ei tohi grupeeringust ega selle tegevustest mitte midagi mäletada.</li>
        </ol>
    </div>
@endsection

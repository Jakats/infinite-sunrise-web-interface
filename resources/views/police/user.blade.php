@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="container">
                @component('components.searchBar', ['routeName' => 'policeSearchUser', 'placeholder' => $searchPlaceholder])
                @endcomponent
            </div>
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @component('police.components.secondaryNav', ['identifier' => $user->identifier, 'active' => 'profile'])
                @endcomponent

                 @if(\App\Helpers\WantedHelper::isUserWanted($wanted))
                    <div class="alert alert-danger mt-5" role="alert">
                            {{strtoupper(__('messages.wanted_character'))}}
                    </div>
                @endif
                <div class="row row-eq-height mt-5 d-flex" style="height: 160px;">
                	<div class="col-md-6 mouse-over h-100">
                        @component('components.userDataBox', ['user' => $user, 'profilePicture' => $profilePicture])
                        @endcomponent
        			</div>
                </div>
                <div class="card mt-5">
                	@component(
                        'components.criminalRecordsTable',
                         [
                            'characters' => $characters,
                            'criminalRecords' => $criminalRecords,
                            'actions' => true,
                            'criminalIdentifier' => $user->identifier
                         ]
                    )
                        @slot('createRecordButton')
                            <form method="POST" action="{{route('createCriminalRecord')}}" class="d-inline-block">
                                @csrf
                                <input type="hidden" name="criminalIdentifier" value="{{$user->identifier}}">
                                <input type="hidden" name="officerIdentifier"
                                       value="{{\Illuminate\Support\Facades\Auth::user()->FiveMUser()->first()->identifier}}">
                                <button type="submit" class="btn btn-info mb-3" onclick="overlayOn()">{{__('buttons.create_new_record')}}</button>
                            </form>
                            <form method="GET"
                                  action="{{route('policeSingleUser', [$user->identifier, true])}}"
                                  class="d-inline-block">
                                <button type="submit" class="btn btn-info mb-3" onclick="overlayOn()">{{__('buttons.get_archive')}}</button>
                            </form>
                        @endslot
                    @endcomponent
                    @component('components.licenses', ['licenses' => $licenses, 'identifier' => $user->identifier])
                    @endcomponent
                    @component('components.bills', ['bills' => $bills, 'identifier' => $user->identifier, 'restrict' => true, 'societyTpe' => 'society_police', 'collapse' => true])
                    @endcomponent
                    @component('components.properties', ['ownedProperties' => $ownedProperties, 'identifier' => $user->identifier])
                    @endcomponent
                    @component('components.characters', ['characters' => $characters, 'identifier' => $user->identifier, 'actions' => true, 'police' => true, 'wanted' => $wanted])
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
@endsection

<script>

    /**
     *
     * @returns {boolean}
     * @constructor
     */
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to continue?");
        if (x)
            return true;
        else
            return false;
    }

</script>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <p><b>Legend:</b></p>
                <ul>
                    @foreach($ranks as $rank)
                        <li>{{$rank->grade}} => {{$rank->label}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="card mt-5">
            @component('components.workersTable', ['routeName' => 'policeSingleUser', 'users' => $users, 'ranks' => $ranks, 'auxiliaryPolice' => $auxiliaryPolice])
            @endcomponent
            @if(method_exists($users, 'links'))
                <div class="container">
                    <div class="pagination justify-content-center p-4">
                        {{$users->links()}}
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

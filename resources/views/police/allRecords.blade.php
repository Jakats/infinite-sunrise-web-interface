@extends('layouts.app')

@section('content')
<div class="container">
    @component(
    'components.searchBar',
    [
    'routeName' => 'policeSearchReports',
    'placeholder' => __('forms.search_records'),
    'oldSearch' => (isset($oldSearch)) ? $oldSearch : null
    ]
    )
    @endcomponent
    <div class="card mt-4">
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">{{__('tables.character_name')}}</th>
                    <th>{{__('tables.jail_time')}}</th>
                    <th>{{__('tables.summary')}}</th>
                    <th>{{__('tables.officer_name')}}</th>
                    <th>{{__('tables.created_at')}}</th>
                    <th>{{__('tables.actions')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($records as $record)
                <tr>
                    <td>{{ $record->character_name}}</td>
                    <td>{{ $record->jail_time}}</td>
                    <td>
                        @if(strlen($record->summary) > 250)
                        {{substr($record->summary, 0, 250)}}
                        @else
                        {{$record->summary}}
                        @endif
                    </td>
                    <td>{{ \App\Helpers\UserHelper::getCharacterName($record->policeOfficer()->first()->user()->first())}}</td>
                    <td>{{ $record->created_at}}</td>
                    <td>
                        <a class="btn btn-info" href="{{route('policeShowReport', [$record->id])}}"
                           onclick="overlayOn()">
                            {{__('buttons.open')}}
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @if(method_exists($records, 'links'))
        <div class="container">
            <div class="pagination justify-content-center p-4">
                {{$records->links()}}
            </div>
        </div>
        @endif
    </div>
</div>
@endsection

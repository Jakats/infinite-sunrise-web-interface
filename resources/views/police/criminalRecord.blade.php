@extends('layouts.app')
@section('content')
    <div class="container">
        @component('components.searchBar', ['routeName' => 'policeSearchUser', 'placeholder' => __('forms.search_user')])
        @endcomponent
        <h2 class="text-gray">Criminal Record #{{$criminalRecord->id}}</h2>
        <div class="card mt-5">
        <div class="card-body">
        @if($criminalRecord->is_criminal_case and new DateTime($criminalRecord->criminal_case_expires_at) > new DateTime())
            <div class="alert alert-danger">Active criminal record</div>
        @endif
            <p onclick="
                    window.location='{{route('policeSingleUser', [$criminalRecord->user_identifier])}}';
                    overlayOn();"
                class="mouse-over"
            >
                <b>{{__('forms.name')}}:</b> {{$criminalRecord->character_name}}
        </p>
        <p>
            <b>{{__('headers.fines')}}:</b>

            @if(isset($criminalRecord->fine))
                @foreach( json_decode($criminalRecord->fine) as $fine )
                    <p>{{ $fine->label }} {{ $fine->amount }}€</p>
                @endforeach
            @endif
        </p>
            <p><b>{{__('forms.jail_time')}}:</b> {{$criminalRecord->jail_time}}</p>
            <p><b>Warnings:</b>
            <ul>
                @if($criminalRecord->warning_car)
                    <li>{{__('forms.make_warning_about_car')}}</li>@endif
                @if($criminalRecord->warning_driver_license)
                    <li>{{__('forms.make_warning_about_traffic_violation_drives_license')}}</li>@endif
                @if($criminalRecord->warning_gun_license)
                    <li>{{__('forms.make_warning_about_gun_law_violation_gun_licence')}}</li>@endif
            </ul>
            </p>
            <p><b>{{__('forms.place')}}: </b>{{$criminalRecord->place}}</p>
            <p><b>{{__('forms.car_plate')}}: </b>{{strtoupper($criminalRecord->car_plate)}}</p>
            <p><b>{{__('texts.is_this_criminal_case')}}: </b>{{($criminalRecord->is_criminal_case) ?__('texts.yes') : __('texts.no')}}</p>
            @if($criminalRecord->is_criminal_case)
                <p><b>Expires at: </b>{{$criminalRecord->criminal_case_expires_at}}</p>
            @endif
            <p><b>{{__('forms.other_parties')}}:</b> {{$criminalRecord->other_parties}}</p>
            <p><b>{{__('forms.summary') }}:</b> {{$criminalRecord->summary}}</p>
            <p><b>{{__('tables.issuer')}}:</b>
                {{\App\Helpers\UserHelper::getCharacterName($criminalRecord->policeOfficer()->first()->user()->first())}}
            </p>
            <a class="btn btn-info d-inline-block" href="{{URL::previous()}}" onclick="overlayOn()">
                {{__('buttons.back_to_previous_page')}}
            </a>
            @if(
                \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job === 'police'
                or \Illuminate\Support\Facades\Auth::user()->hasRole('admin')
            )
                <a href="{{route('showEditCriminalRecord', ['id' => $criminalRecord->id])}}" class="btn btn-info">
                    {{__('buttons.edit')}}
                </a>
            @endif
            @if(
                \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job === 'police'
                or \Illuminate\Support\Facades\Auth::user()->hasRole('admin')
            )
                <button class="btn btn-danger" data-toggle="collapse"
                        href="#deleteCriminalCase"
                        aria-expanded="false"
                        aria-controls="deleteCriminalCase">
                    {{__('buttons.delete')}}</button>
                <form method="POST" action="{{route('policeDeleteReport')}}" id="deleteCriminalCase"
                      class="collapse mt-3 mb-3">
                    @csrf
                    <input type="hidden" name="criminalRecordId" value="{{$criminalRecord->id}}">
                    <input type="hidden" name="loggedInOfficerId"
                           value="{{\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->identifier}}">
                    <input type="text" placeholder="reason">
                    <button type="submit" class="btn btn-danger" onclick="overlayOn()">
                        {{__('buttons.confirm')}}
                    </button>
                </form>
            @endif
            </div>
        </div>
    </div>
@endsection
<script>

    /**
     *
     * @returns {boolean}
     * @constructor
     */
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>

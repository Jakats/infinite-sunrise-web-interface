<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons
    |--------------------------------------------------------------------------
    |
    | This file contains translations of buttons
    |
    */
    'user_data' => 'Kasutaja andmed',
    'bills' => 'Arved',
    'licenses' => 'Load ja litsensid',
    'properties' => 'Kinnisvara',
    'criminal_records' => 'Karistusregister',
    'characters' => 'Karakterid',
    'user_cars' => 'Kasutaja autod',
    'medical_history' => 'Meditsiinilised andmed',
    'ems' => 'Kiirabi',
    'police' => 'Politsei',
    'application' => 'Avaldus',
    'police_job_application' => 'Politsei töö avaldus',
    'ems_job_application' => 'Kiirabi töö avaldus',
    'fines' => 'Trahvid',
    'application_data' => 'Taotluse andmed',
    'actions' => 'Tegevused',
    'car_sale_confirmation' => 'Auto müügi kinnitamine',
    'login' => 'Logi sisse',
    'reset_password' => 'Parooli lähtestamine',
    'whitelist_application_with_web_user_registration' => 'Whitelist-i avaldus',
    'fill_criminal_record_for' => 'Täida raport isiku kohta:',
    'money' => 'Raha',
    'update_profile' => 'Update profile',
    'car_data' => 'Auto andmed',
    'help' => 'Veebilehel legend',
    'whitelist_application_info' => 'Whitelist-i avalduse info',
    'password_reset' => 'Parooli lähtestamine',
    'mechanic_new_report' => 'Uus sissekanne',
    'mechanic_my_report' => 'Minu sissekanded',
    'ban_reasons' => 'Bani põhjused',
];

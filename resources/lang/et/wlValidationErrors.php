<?php

return [
    'name_required' => 'Nimi on kohustuslik',
    'name_min' => 'Nimel peab olema ees- ja perekonna nimi',
    'name_max' => 'Nimi ei tohiks olla pikem, kui 20 tähemärki',
    'character_required' => 'Karakteri nimi on kohstuslik',
    'character_min' => 'Karakteri nimel peab olema ees- ja perekonna nimi',
    'character_max' => 'Karakteri nimi ei tohiks olla pikem, kui 20 tähemärki',
    'steam_required' => 'Steam-i nimi on kohustuslik',
    'steam_min' => 'Steam-i nimi peab olema vähemalt 5 tähemärki',
    'steam_max' => 'Steam-i nimi ei tohiks olla pikem, kui 20 tähemärki',
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons
    |--------------------------------------------------------------------------
    |
    | This file contains translations of buttons
    |
    */

    'label'=> 'Nimetus',
    'issuer'=> 'Väljastaja',
    'receive'=> 'Saaja',
    'amount'=> 'Summa',
    'actions'=> 'Tegevused',
    'character_name' => 'Karakteri nimi',
    'date_of_birth' => 'Sünniaeg',
    'is_wanted' => 'Tagaotsitav',
    'wanted_reason' => 'Tagaotsimise põhjus',
    'declared_wanted_by' => 'Tagaotsitavaks kuulutaja',
    'date' => 'Kuupäev',
    'picture' => 'Pilt',
    'number_plate' => 'Numbrimärk',
    'model_name' => 'Mudeli nimi',
    'is_insured' => 'Kindlustus',
    'is_stolen' => 'Varastatus',
    'insurance_end_date' => 'Kindlustuse lõppkuupäev',
    'sell_car' => 'Automüük',
    'sell_request_nr' => 'Müügi avalduse nr.',
    'jail_time' => 'Vangla karistuse pikkus',
    'summary' => 'Kokkuvõte',
    'is_criminal_case' => 'Kriminaalasi',
    'created_at' => 'Loodud',
    'criminal_case_expires_at' => 'Krimiasja aegumiskuupäev',
    'owner_steam_name' => 'Omaniku steam-i nimi',
    'owner_name' => 'Omaniku nimi',
    'officer_name' => 'Politsei nimi',
    'description' => 'Kirjeldus',
    'category' => 'Kategooria',
    'min_jail' => 'Min vangistus',
    'max_jail' => 'Max vangistus',
    'steam_name' => 'Steam-i nimi',
    'job' => 'Töö',
    'sex' => 'Sugu',
    'request_nr' => 'Avalduse nr.',
    'seller' => 'Müüja',
    'buyer' => 'Ostja',
    'approver' => 'Kinnitaja',
    'name' => 'Nimi',
    'model' => 'Mudel',
    'price' => 'Hind',
    'last_rejection_reason' => 'Viimase keeldumise põhjus',
    'steam_id' => 'Steam-i id',
    'job_type' => 'Töö tüüp',
    'job_price' => 'Töö hind',
    'service_type' => 'Teenust osutati',
    'worker' => 'Töötaja',
    'com_service_min' => 'Min ÜKT',
    'com_service_max' => 'Max ÜKT',
    'total' => 'Summa kokku',
    'phone' => 'Telefoninumber',
    'job_grade' => 'Amet',
    'call_sign' => 'Kutsung',
];

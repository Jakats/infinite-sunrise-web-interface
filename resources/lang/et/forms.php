<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons
    |--------------------------------------------------------------------------
    |
    | This file contains translations of buttons
    |
    */
    'e_mail_address' => 'E-Maili aadress',
    'password' => 'Parool',
    'confirm_password' => 'Parooli kinnitus',
    'remember_me' => 'Jää sisselogituks',
    'buyer_name' => 'Ostja nimi',
    'name' => 'Nimi',
    'character_name' => 'Karakteri nimi',
    'steam_name' => 'Steam-i nimi',
    'steam_hex' => 'Steam-i hex',
    'jail_time' => 'Vanglakaristuse pikkus',
    'make_warning_about_car' => 'Hoiatus auto kohta',
    'make_warning_about_traffic_violation_drives_license' => 'Hoiatus Liiklusrikkumise kohta',
    'make_warning_about_gun_law_violation_gun_licence' => 'Hoaitus relvaseaduse rikkumise kohta',
    'it_s_criminal_case' => 'Juthum on kriminaalne',
    'it_s_not_criminal_case' => 'Juhtum ei ole kriminaalne',
    'summary' => 'Kokkuvõte',
    'profile_picture' => 'Passipilt',
    'search_user' => 'Otsi kasutajat',
    'search_car' => 'Otsi autosid numbrimärgi järgi',
    'search_fine' => 'Otsi trahve',
    'search_records' => 'Otsi kirjeid id, karistatud isiku nime või kirje loonud politseiniku nime järgi',
    'search_car_sell_request' => 'Otsi auto müügi avaldust auto numbrimärgi või müügi avalduse numbri järgi',
    'car_model_name' => 'Auto mudeli nimi',
    'reason' => 'Põhjus',
    'picture' => 'Pilt',
    'description' => 'Kirjeldus',
    'mechanic_job_type' => 'Töö tüüp',
    'mechanic_car_trasnport' => 'Autode transport',
    'mechanic_car_fix' => 'Auto parandus',
    'mechanic_car_tuning' => 'Auto tuunimine',
    'mechanic_car_deals' => 'Sõidukitega seotud tehingud',
    'mechanic_job_price' => 'Töö hind',
    'mechainc_npc' => 'Kohalik',
    'mechainc_citizen' => 'Kodanik',
    'mechainc_ambulance' => 'Kiirabi',
    'mechainc_police' => 'Politsei',
    'mechainc_call' => 'Politsei väljakutse',
    'car_plate' => 'Auto numbrimärk',
    'whenYouCanUseWeapons' => 'Millises olukorras võite kasutada relva?',
    'newLifeRule' => 'Mis juhtub teiega, kui olete haavatud ning jooksete verest tühjaks ehk siis kiirabi teid ei elusta?',
    'helmetMaskRules' => 'Kirjelda millal on lubatud kasutada kiivrit ja millal maski?',
    'robbingOtherCitizen' => 'Millisel hetkel on lubatud röövida teist mängijat, mis hetkel NPCd?',
    'carTax' => 'Millal on kohustus tasuda automaksu?',
    'vdmRdm' => 'Seleta lahti oma sõnadega VDM, RDM, Copbait',
    'oocIc' => 'Seleta lahti OOC, IC',
    'radioUsage' => 'Kellel on lubatud kasutada raadiot (discordi)?',
    'call_sign' => 'Kutsung',
    'place' => 'Asukoht',
    'other_parties' => 'Teised osapooled',
];

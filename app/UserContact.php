<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_contacts';
}

<?php

namespace App\Helpers;

/**
 * Class BanHelper
 */
class StringHelper
{

    public static function trimString($string)
    {
        return strtolower(preg_replace('/\s+/', '',$string));
    }
}

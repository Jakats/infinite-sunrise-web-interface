<?php

namespace App\Console\Commands;

use App\AddonAccountData;
use App\AddonInventoryItem;
use App\Bill;
use App\CarInsurance;
use App\CarSellLogs;
use App\CarSellRequest;
use App\Character;
use App\CharacterKillLog;
use App\ConfiscateCarLog;
use App\CriminalRecord;
use App\DatastoreData;
use App\Helpers\Sql\CharacterKillHelper;
use App\JobApplication;
use App\MedicalHistory;
use App\OwnedCar;
use App\OwnedMotel;
use App\OwnedProperty;
use App\PhoneAppChat;
use App\PhoneCall;
use App\PhoneMessage;
use App\PhoneUserContact;
use App\StolenCar;
use App\User;
use App\UserAccount;
use App\UserContact;
use App\UserInventory;
use App\UserLicenses;
use App\WantedCar;
use App\WantedCharacters;
use App\WebUser;
use App\Whitelist;
use App\WhitelistApplications;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DeleteAllUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:deleteAll';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all users and user related data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Start deleting users');

        UserLicenses::truncate();
        WantedCar::truncate();
        WantedCharacters::truncate();
        User::truncate();
        UserInventory::truncate();
        UserContact::truncate();
        UserAccount::truncate();
        StolenCar::truncate();
        PhoneUserContact::truncate();
        PhoneMessage::truncate();
        PhoneCall::truncate();
        PhoneAppChat::truncate();
        OwnedCar::truncate();
        //OwnedMotel::truncate();
        JobApplication::truncate();
        DatastoreData::truncate();
        ConfiscateCarLog::truncate();
        CharacterKillLog::truncate();
        Character::truncate();
        CarSellLogs::truncate();
        CarSellRequest::truncate();
        CarInsurance::truncate();
        AddonInventoryItem::truncate();
        AddonAccountData::truncate();
        CriminalRecord::truncate();
        MedicalHistory::truncate();
        OwnedProperty::truncate();
        WantedCharacters::truncate();
        Bill::truncate();

        DB::table('user_web_user')->truncate();
        DB::table('vehicle_sold')->truncate();
        DB::table('owned_dock')->truncate();

        $this->info('Deleted');
    }
}

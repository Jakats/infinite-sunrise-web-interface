<?php

namespace App\Http\Controllers;

use App\Helpers\Sql\FivemUserHelper;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function userSearchAutocomplete(Request $request)
    {

        $data = FivemUserHelper::searchUsers($request->input('query'));

        $sortedArray = [];
        $counter = 0;
        foreach ($data as $item) {
            $sortedArray[$counter]['name'] = $item->firstname . ' ' . $item->lastname;
            $counter++;
        }

        return response()->json($sortedArray);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function carPlateSearchAutocomplete(Request $request)
    {


        $data = DB::table('owned_vehicles')
            ->whereRaw('LOWER(plate) like :plate', ['plate' => '%' . strtolower($request->input('query')) . '%'])
            ->get(['plate']);

        $sortedArray = [];
        $counter = 0;
        foreach ($data as $item) {
            $sortedArray[$counter]['name'] = $item->plate;
            $counter++;
        }

        return response()->json($sortedArray);
    }
}

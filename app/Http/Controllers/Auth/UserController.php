<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Role;
use App\WebUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserController
 * @package App\Http\Controllers\Auth
 */
class UserController extends Controller
{

    public function create()
    {
        return view('auth.register');
    }

    public function store()
    {

        request()->validate([

            'name' => 'required|min:2|max:50',

            'email' => 'required|email|unique:web_users',

            'password' => 'required|min:6|confirmed',

            'fiveMUser' => 'required|string'

        ], [

            'name.required' => 'Name is required',

            'name.min' => 'Name must be at least 2 characters.',

            'name.max' => 'Name should not be greater than 50 characters.',

        ]);

        $fiveMUserId = DB::table('users')
            ->where('name', '=', \request('fiveMUser'))
            ->get(['identifier']);

        if (!$fiveMUserId->count()) {
            return redirect()->back()->withErrors(['fiveMUser' => ['message' => 'FiveM User does not exist']])
                ->withInput(\request()->all());
        } elseif ($fiveMUserId->count() > 1) {
            return redirect()->back()->withErrors(
                ['fiveMUser' =>
                    [
                        'message' => 'There are more then 1 FiveM user named ' . \request('fiveMUser')
                            . '. Please change your FiveM/Steam name, login into gama and come back to finish your user creation.'
                    ]
                ]
            )->withInput(\request()->all());
        }

        if (
            DB::table('user_web_user')
                ->where('user_id', '=', $fiveMUserId->first()->identifier)
                ->get()
                ->count()
        ) {
            return redirect()->back()->withErrors(
                ['fiveMUser' =>
                    [
                        'message' => 'This FiveM user already have an account'
                    ]
                ]
            )->withInput(\request()->all());
        }

        $user = WebUser::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => Hash::make(\request('password')),
        ]);
        $user
            ->roles()
            ->attach(Role::where('name', 'player')->first());
        $user->fiveMUser()->attach($fiveMUserId->first()->identifier);

        Auth::loginUsingId($user->id);
        return redirect('/home');
    }
}
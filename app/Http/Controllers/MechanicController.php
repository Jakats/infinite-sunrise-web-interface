<?php
namespace App\Http\Controllers;

use App\Helpers\BillsHelper;
use Exception;

use App\MechanicReports;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use PHPUnit\Framework\RiskyTestError;

/**
* Mechanic controller
*/
class MechanicController extends Controller
{

    /**
    * @var array
    */
    const ALLOWED_JOBS = [
        'mechanic',
        'offmechanic'
    ];

    /**
    * @var array
    */
    const ALLOWED_JOB_GRADES = [
        99
    ];

    /**
    * Job types for mechanic new report view
    * value => translation
    * @var array
    */
    const JOB_TYPE = [
        'carTransport' => 'mechanic_car_trasnport',
        'carFix' => 'mechanic_car_fix',
        'carTuning' => 'mechanic_car_tuning',
        'carDeals' => 'mechanic_car_deals',
    ];

    /**
    * Service types for mechanic new report view
    * value/id => translation
    * @var array
    */
    const SERVICE_TYPE = [
        'npc' => 'mechainc_npc',
        'citizen' => 'mechainc_citizen',
        'ambulance' => 'mechainc_ambulance',
        'police' => 'mechainc_police',
        'policeCall' => 'mechainc_call',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    public function newReport(Request $request)
    {

        $this->restrictAccess();
        return view(
            'mechanic.newReport',
             [
                 'jobType' => self::JOB_TYPE,
                 'serviceType' => self::SERVICE_TYPE
             ]
         );
    }

    public function saveNewReport(Request $request)
    {

        try {
            $this->restrictAccess();
            $newReport = new MechanicReports();
            $newReport->user_identifier = Auth::user()->fiveMUser()->first()->identifier;
            $newReport->job_type = $request->jobType;
            $newReport->job_price = $request->jobPrice;
            $newReport->car_plate = $request->carPlate;
            $newReport->service_type = $request->serviceType;
            $newReport->summary = $request->summary;
            $newReport->save();

            return redirect(route('mechanicMyReports'));
        } catch (Exception $exception) {
            throw new \Exception($exception->getMessage(), $exception->getCode());

        }
    }

    public function myReports(Request $request)
    {
        try {
            $this->restrictAccess();
            return view(
                'mechanic.myReports',
                 [
                     'reports' => Auth::user()->fivemUser()->first()->mechanicReports
                 ]
             );
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage(), $exception->getCode());

        }
    }

    public function allReports(Request $request)
    {
        try {
            $this->restrictAccess();
            $this->restrictAccessForBoss();

            return view(
                'mechanic.allReports',
                 [
                     'reports' => MechanicReports::with('user')->orderBy('id', 'DESC')->paginate(20)
                 ]
             );
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage(), $exception->getCode());

        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allBills(Request $request)
    {

        $this->restrictAccess();

        return view(
            'common.bills',
            BillsHelper::getSocietyBills('society_mechanic')
        );
    }

    /**
     * Control User job in Server and if not police or fib, then dont allow to continue
     */
    protected function restrictAccess()
    {

        $loggedInUserData = Auth::user()->fiveMUser()->first();

        $isAdmin = Auth::user()->hasRole('admin');
        if (!in_array($loggedInUserData->job, self::ALLOWED_JOBS) and !$isAdmin) {
            abort(401, 'This action is unauthorized.');
        }

        return;
    }

    /**
     * Control User job in Server and if not police or fib, then dont allow to continue
     */
    protected function restrictAccessForBoss()
    {

        $loggedInUserData = Auth::user()->fiveMUser()->first();

        $isAdmin = Auth::user()->hasRole('admin');
        if (!in_array($loggedInUserData->job_grade, self::ALLOWED_JOB_GRADES) and !$isAdmin) {
            abort(401, 'This action is unauthorized.');
        }

        return;
    }
}

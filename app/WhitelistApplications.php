<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhitelistApplications extends Model
{

    public function webUser()
    {
        return $this->belongsTo(WebUser::class);
    }

    public function user()
    {

        return $this->belongsTo(User::class);
    }
}
